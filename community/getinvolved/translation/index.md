---
title: 翻訳
---

## はじめに

KDE では、アプリケーションの翻訳に協力してくださる方からの翻訳ファイルを常に受け付けています。

翻訳手順の概要は次の通りです。

1. 翻訳ファイルをダウンロード
2. 翻訳
3. 翻訳ファイルを提出

以下、もう少し詳しく説明していきます。

## 翻訳ファイル

翻訳ファイルはテキストファイルで、PO というフォーマットになっています。
翻訳作業中は、そのフォーマットを崩さないようにする必要があります。

まず、先頭に

```
msgid ""
msgstr ""
"Project-Id-Version: kateproject\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2019-03-18 10:00+0100\n"
"PO-Revision-Date: 2012-08-15 23:01-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
```

というフォーマットのヘッダーがあり、その後

```
#. i18n: ectx: Menu (file)
#. +> trunk5 stable5
#: data/kateui.rc:6
#, kde-format
msgid "&File"
msgstr "ファイル(&F)"
```

というフォーマットが繰り返されます。

翻訳作業は、`msgstr` の後に日本語訳を設定するだけです。他の部分を変更したり、削除したりしないでください。特に、`#` で始まる行はコメントですが、出典を示したり、同じ `msgid` を区別したりする情報を含んでいるので、必ず残してください。

## ファイルの入手

日本語の翻訳では、複数のリリースに別れた翻訳ファイルを一つにまとめた Summit と呼ばれるファイルを翻訳しています。
Summit のファイルは、以下のリンクから入手することができます。

- [https://websvn.kde.org/trunk/l10n-support/ja/summit/messages/](https://websvn.kde.org/trunk/l10n-support/ja/summit/messages/)

### 翻訳ファイルの選択

2019年4月現在、翻訳するファイルは、翻訳者自身の関心や興味に基づいて選んでもらっています。

ちなみに、過去には、協力者を募り、こちらからファイルを指定したり、「翻訳予約表」を運用したりしていた時期もありましたが、現在はしていません。

## 翻訳作業

翻訳ファイルの編集にあたっては、使い慣れたエディターを使ってもらって問題ありません。ただし、テキストファイルとして保存できるものでなければなりません。

翻訳作業中は、PO フォーマットを崩すことなく、`msgid` の原文、また、その上にある文脈情報などを参考にして、日本語訳を `msgstr` に設定してください。
また、翻訳が終了した `msgstr` については、コメント中に `fuzzy` がある場合、それのみを取り除いてください。

例) `msgstr` の翻訳が終わりました。コメント中に `fuzzy` があります。

```
#. +> trunk5
#: declarativeimports/calendar/qml/MonthView.qml:284
#, fuzzy, kde-format
msgid "Next Year"
msgstr "次の年"
```

`fuzzy` を含む行に他のキーワードがない場合、その行を削除しますが、上の例のように他のキーワードが含まれた場合、`fuzzy` だけを取り除きます。

```
#. +> trunk5
#: declarativeimports/calendar/qml/MonthView.qml:284
#, kde-format
msgid "Next Year"
msgstr "次の年"
```

### 複数形

日本語に複数形はありませんが、KDE の日本語訳では、より自然な翻訳を求めて、原文に複数形表現がある場合、日本語訳にも複数形表現を設定しています。複数形表現がある場合、原文には `msgid` 以外に `msgid_plural` があります。この場合、日本語訳は単数形表現を `msgstr[0]` に設定し、複数形表現を `msgstr[1]` に設定します。

## 翻訳済みファイルの提出

翻訳ファイルは、すべての項目について翻訳が終っていなくても受け付けます。また、仮に長期的に作業したい場合でも、小まめに提出してもらえると助かります。(例えば週に一度とか)

まず、一つのアーカイブファイル(例えば ZIP とか)にまとめてください。

実際の提出方法には二通りあります。

### メールで提出

翻訳作業が終った旨 kde-jp@kde.org へメールを送るときに、アーカイブファイルを添付してください。このとき、新しいスレッドを作ってください。

### collaborate.kde.org へアップロード

KDE が運営する collaborate.kde.org (要アカウント)へアーカイブファイルをアップロードし、翻訳作業が終った旨 kde-jp@kde.org へメールを送るときに、アップロードしたファイルへのリンクを記してください。このとき、新しいスレッドを作ってください。

## より詳しい情報

PO フォーマットに関する詳しい説明 (英文)
[Localization/Concepts/PO Odyssey](https://techbase.kde.org/Localization/Concepts/PO_Odyssey)

Summit を使った翻訳プロセスに関する詳しい説明 (英文)
[Localization/Workflows/PO Summit](https://techbase.kde.org/Localization/Workflows/PO_Summit)
